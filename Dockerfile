FROM maven:3-amazoncorretto-17-debian-bookworm as builder

COPY src/ src/
COPY pom.xml pom.xml
RUN mvn clean package



FROM openjdk:17-ea-jdk-slim

COPY --from=builder target/apresentacao.jar app.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "app.jar"]